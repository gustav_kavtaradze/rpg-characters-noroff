package Item;

public abstract class Item {

    private String name;
    private int reqLevel;
    private ItemType itemType;
    private ItemSlot itemSlot;

    public Item() {
    }

    public Item(String name, int reqLevel, ItemType itemType, ItemSlot itemSlot) {
        this.name = name;
        this.reqLevel = reqLevel;
        this.itemType = itemType;
        this.itemSlot = itemSlot;
    }


    //region gettersSetters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getReqLevel() {
        return reqLevel;
    }
    public void setReqLevel(int reqLevel) {
        this.reqLevel = reqLevel;
    }
    public ItemType getItemType() {
        return itemType;
    }
    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
    public ItemSlot getItemSlot() {
        return itemSlot;
    }
    public void setItemSlot(ItemSlot itemSlot) {
        this.itemSlot = itemSlot;
    }
    //endregion

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", reqLevel=" + reqLevel +
                ", itemType=" + itemType +
                ", itemSlot=" + itemSlot +
                '}';
    }
}
