package Item;

import Hero.PrimaryAttributes;

public class Armor extends Item{

    private PrimaryAttributes primaryAttributes;

    public Armor() {
    }

    public Armor(String name, int reqLevel, ItemType itemType, ItemSlot itemSlot, PrimaryAttributes primaryAttributes) {
        super(name, reqLevel, itemType, itemSlot);
        this.primaryAttributes = primaryAttributes;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }
    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }

    @Override
    public String toString() {
        return "Armor{" +
                "primaryAttributes=" + primaryAttributes +
                '}';
    }
}
