package Item;

public class Weapon extends Item {

    private int damage;
    private double attackSpeed;

    public Weapon() {
        this.setItemSlot(ItemSlot.WEAPON);
    }

    public Weapon(String name, int reqLevel, ItemType itemType, ItemSlot itemSlot, int damage, double attackSpeed) {
        super(name, reqLevel, itemType, itemSlot);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    /**
     * Makes calculation to make weapon damage per second
     * @return dps
     */
    public double getDPS(){
        return damage*attackSpeed;
    }

    //region gettersSetters
    public int getDamage() {
        return damage;
    }
    public void setDamage(int damage) {
        this.damage = damage;
    }
    public double getAttackSpeed() {
        return attackSpeed;
    }
    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
    //endregion


    @Override
    public String toString() {
        return "Weapon{" +
                "damage=" + damage +
                ", attackSpeed=" + attackSpeed +
                '}';
    }
}
