package Hero;

import Item.Item;
import Item.ItemType;
import Item.Armor;

import java.util.ArrayList;
import java.util.List;

public class Warrior extends Hero {

    public Warrior() {
        this.setBasePrimaryAttributes(new PrimaryAttributes(5, 2, 1));
        this.setHeroCanHaveItem(new ArrayList<>(List.of(
                ItemType.AXE, ItemType.HAMMER, ItemType.SWORD, ItemType.MAIL_ARMOUR, ItemType.PLATE_ARMOR)));
    }

    /**
     * Overriding method from parent to send back that attribute determines hero damage
     *
     * @return int of attribute
     */
    @Override
    public int getHeroPrimaryAttribute() {
        int primaryAttribute = this.getBasePrimaryAttributes().getStrength();
        for (Item item : getEquiptItems().values()) {
            if (item instanceof Armor)
                primaryAttribute += ((Armor) item).getPrimaryAttributes().getStrength();
        }
        return primaryAttribute;
    }

    /**
     * Overriding method to specify how level up works on child
     */
    @Override
    public void levelUp() {
        this.setLevel(this.getLevel() + 1);
        this.getBasePrimaryAttributes().addAttributes(3, 2, 1);
    }

}
