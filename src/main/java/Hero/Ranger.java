package Hero;

import Item.Item;
import Item.ItemType;
import Item.Armor;

import java.util.ArrayList;
import java.util.List;

public class Ranger extends Hero {

    public Ranger() {
        this.setBasePrimaryAttributes(new PrimaryAttributes(1, 7, 1));
        this.setHeroCanHaveItem(new ArrayList<>(List.of(
                ItemType.BOW, ItemType.LEATHER_ARMOUR, ItemType.MAIL_ARMOUR)));
    }

    /**
     * Overriding method from parent to send back that attribute determines hero damage
     *
     * @return int of attribute
     */
    @Override
    public int getHeroPrimaryAttribute() {
        int primaryAttribute = this.getBasePrimaryAttributes().getDexterity();
        for (Item item : getEquiptItems().values()) {
            if (item instanceof Armor)
                primaryAttribute += ((Armor) item).getPrimaryAttributes().getDexterity();
        }
        return primaryAttribute;
    }

    /**
     * Overriding method to specify how level up works on child
     */
    @Override
    public void levelUp() {
        this.setLevel(this.getLevel() + 1);
        this.getBasePrimaryAttributes().addAttributes(1, 5, 1);
    }
}
