package Hero;

import Item.Item;
import Item.ItemType;
import Item.Armor;

import java.util.ArrayList;
import java.util.List;

public class Mage extends Hero {

    public Mage() {
        this.setBasePrimaryAttributes(new PrimaryAttributes(1,1,8));
        this.setHeroCanHaveItem(new ArrayList<>(List.of(
                ItemType.STAFF, ItemType.WAND, ItemType.CLOTH_ARMOR)));
    }

    /**
     * Overriding method from parent to send back that attribute determines hero damage
     * @return int of attribute
     */
    @Override
    public int getHeroPrimaryAttribute() {
        int primaryAttribute = this.getBasePrimaryAttributes().getIntelligence();
        for (Item item : getEquiptItems().values()) {
            if (item instanceof Armor)
                primaryAttribute += ((Armor) item).getPrimaryAttributes().getIntelligence();
        }
        return primaryAttribute;
    }

    /**
     * Overriding method to specify how level up works on child
     */
    @Override
    public void levelUp() {
        this.setLevel(this.getLevel()+1);
        this.getBasePrimaryAttributes().addAttributes(1,1,5);
    }
}
