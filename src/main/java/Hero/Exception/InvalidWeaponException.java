package Hero.Exception;

public class InvalidWeaponException extends Exception{

    public InvalidWeaponException(String message) {
        super(message);
    }
}
