package Hero;

import Item.Item;
import Item.ItemType;
import Item.Armor;

import java.util.ArrayList;
import java.util.List;

public class Rogue extends Hero {

    public Rogue() {
        this.setBasePrimaryAttributes(new PrimaryAttributes(2, 6, 1));
        this.setHeroCanHaveItem(new ArrayList<>(List.of(
                ItemType.DAGGER, ItemType.SWORD, ItemType.LEATHER_ARMOUR, ItemType.MAIL_ARMOUR)));
    }

    /**
     * Overriding method from parent to send back that attribute determines hero damage
     *
     * @return int of attribute
     */
    @Override
    public int getHeroPrimaryAttribute() {
        int primaryAttribute = this.getBasePrimaryAttributes().getDexterity();
        for (Item item : getEquiptItems().values()) {
            if (item instanceof Armor)
                primaryAttribute += ((Armor) item).getPrimaryAttributes().getDexterity();
        }
        return primaryAttribute;
    }

    /**
     * Overriding method to specify how level up works on child
     */
    @Override
    public void levelUp() {
        this.setLevel(this.getLevel() + 1);
        this.getBasePrimaryAttributes().addAttributes(1, 4, 1);
    }
}
