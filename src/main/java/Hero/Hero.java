package Hero;

import Hero.Exception.InvalidArmorException;
import Hero.Exception.InvalidWeaponException;
import Item.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hero {

    //region props
    private String name;
    private int level;

    private PrimaryAttributes basePrimaryAttributes;
    private PrimaryAttributes totalPrimaryAttributes;

    private List<ItemType> heroCanHaveItem;
    private Map<ItemSlot, Item> equiptItems;
    //endregion

    public Hero() {
        this.level = 1;
        this.equiptItems = new HashMap<>();
    }

    /**
     * Abstract method to make child classes handel how level up should work
     * on child class / hero
     */
    public abstract void levelUp();

    /**
     * Method to equip a new item to hero by adding to Map,
     * Uses List heroCanHaveItem to determent if it item can be added.
     * @param item new item to add
     * @return true if equipt succeded
     * @throws InvalidWeaponException if somting wrong with weapon
     * @throws InvalidArmorException if somting wrong with armour
     */
    public boolean equipItem(Item item) throws InvalidWeaponException, InvalidArmorException {
        if (heroCanHaveItem.contains(item.getItemType()) && level >= item.getReqLevel()) {
            getEquiptItems().put(item.getItemSlot(), item);
            return true;
        }
        else if (item.getItemSlot() == ItemSlot.WEAPON)
            throw new InvalidWeaponException("Cannot equip weapon");
        else
            throw new InvalidArmorException("Cannot equip armor");
    }

    /**
     * Method to calculate the dps of the made character, uses calculation
     *  Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
     *  Where TotalMainPrimaryAttribute includes character armour.
     * @return Character DPS
     */
    public double calculateDPS(){
        Weapon weapon = (Weapon) equiptItems.get(ItemSlot.WEAPON);
        if(weapon != null)
            return weapon.getDPS() *(1 + (getHeroPrimaryAttribute() / 100.0));
        return 1*(1 + (getHeroPrimaryAttribute() / 100.0));
    }

    /**
     * Abstract method to get the heroes primaryAttribute in child classes
     * @return should return int number of hero attribute that increases hero damage
     */
    public abstract int getHeroPrimaryAttribute();

    /**
     * Method to dissplay hero in an easy-to-read way in console
     * @return string of hero
     */
    public String displayStats() {
        int strength = basePrimaryAttributes.getStrength();
        int dexterity = basePrimaryAttributes.getDexterity();
        int intelligence = basePrimaryAttributes.getIntelligence();
        for (Item item : equiptItems.values()) {
            if (item instanceof Armor){
                strength += ((Armor) item).getPrimaryAttributes().getStrength();
                dexterity += ((Armor) item).getPrimaryAttributes().getDexterity();
                intelligence += ((Armor) item).getPrimaryAttributes().getIntelligence();
            }
        }

        StringBuilder builder = new StringBuilder("Character name: ").append(name).append(System.lineSeparator()).
                append("Character level: ").append(level).append(System.lineSeparator());

        builder.append("Stats: ").append(System.lineSeparator());
        builder.append("\tStrength: ").append(strength).append(System.lineSeparator());
        builder.append("\tDexterity: ").append(dexterity).append(System.lineSeparator());
        builder.append("\tIntelligence: ").append(intelligence).append(System.lineSeparator());

        builder.append("Hero dps: ").append(calculateDPS());

        return builder.toString();
    }


    //region getterSetters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
    }

//    public PrimaryAttributes getTotalPrimaryAttributes() {
//        return totalPrimaryAttributes;
//    }
//
//    public void setTotalPrimaryAttributes(PrimaryAttributes totalPrimaryAttributes) {
//        this.totalPrimaryAttributes = totalPrimaryAttributes;
//    }

    public List<ItemType> getHeroCanHaveItem() {
        return heroCanHaveItem;
    }

    public void setHeroCanHaveItem(List<ItemType> heroCanHaveItem) {
        this.heroCanHaveItem = heroCanHaveItem;
    }

    public Map<ItemSlot, Item> getEquiptItems() {
        return equiptItems;
    }

    public void setEquiptItems(Map<ItemSlot, Item> equiptItems) {
        this.equiptItems = equiptItems;
    }

    //endregion

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", basePrimaryAttributes=" + basePrimaryAttributes +
                ", totalPrimaryAttributes=" + totalPrimaryAttributes +
                ", heroCanHaveItem=" + heroCanHaveItem +
                ", equiptItems=" + equiptItems +
                '}';
    }
}
