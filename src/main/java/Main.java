import Hero.Hero;
import Hero.Warrior;

public class Main {

    public static void main(String[] args) {

        Hero hero = new Warrior();
        System.out.println(hero.displayStats());
    }
}
