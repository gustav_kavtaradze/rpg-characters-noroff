package ItemTest;

import Item.ItemSlot;
import Item.Weapon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WeaponTest {


    @Test
    void WeaponSetGetDamage_Damage_DamageChange(){
        Weapon weapon = new Weapon();
        weapon.setDamage(2);
        Assertions.assertEquals(2,weapon.getDamage());
    }

    @Test
    void WeaponSetGetAttackSpeed_AttackSpeed_AttackSpeedChange(){
        Weapon weapon = new Weapon();
        weapon.setAttackSpeed(2);
        Assertions.assertEquals(2,weapon.getAttackSpeed());
    }


    @Test
    void GetDPS_Damage2Speed2_DPS4(){
        Weapon weapon = new Weapon();
        weapon.setDamage(2);
        weapon.setAttackSpeed(2);
        Assertions.assertEquals(4,weapon.getDPS());
    }
    @Test
    void GetDPS_Damage0Speed0_DPS0(){
        Weapon weapon = new Weapon();
        weapon.setDamage(0);
        weapon.setAttackSpeed(0);
        Assertions.assertEquals(0,weapon.getDPS());
    }
    @Test
    void GetDPS_NoSetDamageOrSpeed_DPS0(){
        Weapon weapon = new Weapon();
        Assertions.assertEquals(0,weapon.getDPS());
    }

    @Test
    void WeaponConstructor_ItemSlot_ItemSlotWeapon(){
        Weapon weapon = new Weapon();
        Assertions.assertEquals(ItemSlot.WEAPON,weapon.getItemSlot());
    }
}
