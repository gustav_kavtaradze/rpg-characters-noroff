package ItemTest;

import Hero.*;
import Hero.Exception.InvalidArmorException;
import Hero.Exception.InvalidWeaponException;
import Item.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ItemTest {

    // Naming is important.
    // MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().

    @Test
    void ItemGetSetName_Name_NameChange(){
        Item item = new Weapon();
        item.setName("JUnit");
        Assertions.assertEquals("JUnit",item.getName());
    }

    @Test
    void ItemGetSetReqLevel_Level_LevelChange(){
        Item item = new Weapon();
        item.setReqLevel(5);
        Assertions.assertEquals(5,item.getReqLevel());
    }

    @Test
    void ItemGetSetItemSlot_ItemSlot_ItemSlotChange(){
        Item item = new Armor();
        item.setItemSlot(ItemSlot.BODY);
        Assertions.assertEquals(ItemSlot.BODY,item.getItemSlot());
    }

    @Test
    void ItemGetSetItemType_ItemType_ItemTypeChange(){
        Item item = new Weapon();
        item.setItemType(ItemType.AXE);
        Assertions.assertEquals(ItemType.AXE,item.getItemType());
    }


}
