package ItemTest;

import Hero.PrimaryAttributes;
import Item.Armor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArmorTest {


    @Test
    void ArmorSetGetPrimaryAttributes_PrimaryAttributes_PrimaryAttributesChange(){
        Armor armor = new Armor();
        armor.setPrimaryAttributes(new PrimaryAttributes(1,1,1));
        Assertions.assertEquals(new PrimaryAttributes(1,1,1),armor.getPrimaryAttributes());
    }
}
