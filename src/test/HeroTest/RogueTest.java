package HeroTest;

import Hero.Hero;
import Item.ItemType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import Hero.Rogue;
import Hero.PrimaryAttributes;

public class RogueTest {

    @Test
    void RogueConstructor_Level_LevelOne() {
        Rogue rogue = new Rogue();
        Assertions.assertEquals(1, rogue.getLevel());
    }

    @Test
    void RogueConstructor_HeroCanHaveItem_ContainsList() {
        Rogue rogue = new Rogue();
        ItemType[] itemTypes = {ItemType.DAGGER, ItemType.SWORD, ItemType.LEATHER_ARMOUR, ItemType.MAIL_ARMOUR};
        Assertions.assertArrayEquals(itemTypes, rogue.getHeroCanHaveItem().toArray(new ItemType[0]));
    }

    @Test
    void RogueConstructor_BaseAttributes_Str2Dex6Int1() {
        Hero hero = new Rogue();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(2, 6, 1);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

    @Test
    void RogueLevelUp_BaseAttributes_Str3Dex10Int2() {
        Hero hero = new Rogue();
        hero.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(3, 10, 2);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

    @Test
    void GetRoguePrimaryAttributeRogue_PrimaryAttribute_Intelligence8() {
        Hero hero = new Rogue();
        Assertions.assertEquals(6, hero.getHeroPrimaryAttribute());
    }
}
