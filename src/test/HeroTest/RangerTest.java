package HeroTest;

import Hero.Hero;
import Item.ItemType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import Hero.Ranger;
import Hero.PrimaryAttributes;

public class RangerTest {

    @Test
    public void RangerConstructor_Level_LevelOne() {
        Ranger ranger = new Ranger();
        Assertions.assertEquals(1, ranger.getLevel());
    }

    @Test
    void RangerConstructor_HeroCanHaveItem_ContainsList() {
        Ranger ranger = new Ranger();
        ItemType[] itemTypes = {  ItemType.BOW, ItemType.LEATHER_ARMOUR, ItemType.MAIL_ARMOUR};
        Assertions.assertArrayEquals(itemTypes, ranger.getHeroCanHaveItem().toArray(new ItemType[0]));
    }

    @Test
    public void RangerConstructor_BaseAttributes_Str1Dex7Int1() {
        Hero hero = new Ranger();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(1,7,1);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

    @Test
    public void RangerLevelUp_BaseAttributes_Str2Dex12Int2() {
        Hero hero = new Ranger();
        hero.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(2,12,2);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

    @Test
    void GetRangerPrimaryAttributeRanger_PrimaryAttribute_Intelligence8(){
        Hero hero = new Ranger();
        Assertions.assertEquals(7,hero.getHeroPrimaryAttribute());
    }
}
