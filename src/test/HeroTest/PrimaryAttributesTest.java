package HeroTest;

import Hero.PrimaryAttributes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PrimaryAttributesTest {


    //region getterSettersTest
    @Test
    void PrimaryAttributesSetGetStrength_Strength_StrengthChange(){
        PrimaryAttributes primaryAttributes = new PrimaryAttributes();
        primaryAttributes.setStrength(1);
        Assertions.assertEquals(1,primaryAttributes.getStrength());
    }

    @Test
    void PrimaryAttributesSetGetDexterity_Dexterity_DexterityChange(){
        PrimaryAttributes primaryAttributes = new PrimaryAttributes();
        primaryAttributes.setDexterity(1);
        Assertions.assertEquals(1,primaryAttributes.getDexterity());
    }

    @Test
    void PrimaryAttributesSetGetIntelligence_Intelligence_IntelligenceChange(){
        PrimaryAttributes primaryAttributes = new PrimaryAttributes();
        primaryAttributes.setIntelligence(1);
        Assertions.assertEquals(1,primaryAttributes.getIntelligence());
    }

    @Test
    void PrimaryAttributeEquals_Equals_AreEquals(){
        Assertions.assertEquals(new PrimaryAttributes(1, 1, 1), new PrimaryAttributes(1, 1, 1));
    }
    @Test
    void PrimaryAttributeEquals_Equals_NoTEquals(){
        Assertions.assertNotEquals(new PrimaryAttributes(1, 1, 1), new PrimaryAttributes(1, 1, 2));
    }

    //endregion
}
