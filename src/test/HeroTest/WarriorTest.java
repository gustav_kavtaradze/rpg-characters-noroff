package HeroTest;

import Hero.Hero;
import Item.ItemType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import Hero.Warrior;
import Hero.PrimaryAttributes;

public class WarriorTest {

    @Test
    public void WarriorConstructor_Level_LevelOne() {
        Warrior warrior = new Warrior();
        Assertions.assertEquals(1, warrior.getLevel());
    }

    @Test
    void WarriorConstructor_HeroCanHaveItem_ContainsList() {
        Warrior warrior = new Warrior();
        ItemType[] itemTypes = {ItemType.AXE, ItemType.HAMMER, ItemType.SWORD, ItemType.MAIL_ARMOUR, ItemType.PLATE_ARMOR};
        Assertions.assertArrayEquals(itemTypes, warrior.getHeroCanHaveItem().toArray(new ItemType[0]));
    }

    @Test
    public void WarriorConstructor_BaseAttributes_Str5Dex2Int1() {
        Hero hero = new Warrior();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(5, 2, 1);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

    @Test
    public void WarriorLevelUp_BaseAttributes_Str8Dex4Int2() {
        Hero hero = new Warrior();
        hero.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(8, 4, 2);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

    @Test
    void GetHeroPrimaryAttributeWarrior_PrimaryAttribute_Strength5() {
        Hero hero = new Warrior();
        Assertions.assertEquals(5, hero.getHeroPrimaryAttribute());
    }

}
