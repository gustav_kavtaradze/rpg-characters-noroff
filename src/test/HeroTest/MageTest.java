package HeroTest;

import Hero.Hero;
import Item.ItemType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import Hero.Mage;
import Hero.PrimaryAttributes;

public class MageTest {

    @Test
    public void MageConstructor_Level_LevelOne() {
        Mage mage = new Mage();
        Assertions.assertEquals(1, mage.getLevel());
    }

    @Test
    void MageConstructor_HeroCanHaveItem_ContainsList() {
        Mage mage = new Mage();
        ItemType[] itemTypes = {ItemType.STAFF, ItemType.WAND, ItemType.CLOTH_ARMOR};
        Assertions.assertArrayEquals(itemTypes, mage.getHeroCanHaveItem().toArray(new ItemType[0]));
    }

    @Test
    public void MageConstructor_BaseAttributes_Str1Dex1Int8() {
        Hero hero = new Mage();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(1,1,8);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

    @Test
    void GetMagePrimaryAttributeMage_PrimaryAttribute_Intelligence8(){
        Hero hero = new Mage();
        Assertions.assertEquals(8,hero.getHeroPrimaryAttribute());
    }

    @Test
    public void MageLevelUp_BaseAttributes_Str2Dex2Int13() {
        Hero hero = new Mage();
        hero.levelUp();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(2,2,13);
        Assertions.assertEquals(primaryAttributes, hero.getBasePrimaryAttributes());
    }

}
