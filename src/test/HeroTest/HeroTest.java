package HeroTest;

import Hero.Exception.InvalidArmorException;
import Hero.Exception.InvalidWeaponException;
import Hero.*;
import Item.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class HeroTest {


    //region InvalidWeaponInvalidArmorException
    @Test
    public void HeroEquipItem_ToHighWeaponLevel_InvalidWeaponException() {
        Hero hero = new Warrior();
        Item item = new Weapon("Invalid_Axe", 2, ItemType.AXE, ItemSlot.WEAPON, 0, 0);
        Assertions.assertThrows(InvalidWeaponException.class, () -> hero.equipItem(item));
    }

    @Test
    public void HeroEquipItem_ToHighArmorLevel_InvalidArmorException() {
        Hero hero = new Warrior();
        Item item = new Armor("Invalid_Axe", 2, ItemType.PLATE_ARMOR, ItemSlot.BODY, new PrimaryAttributes());
        Assertions.assertThrows(InvalidArmorException.class, () -> hero.equipItem(item));
    }

    @Test
    public void HeroEquipItem_WrongTypeWeapon_InvalidWeaponException() {
        Hero hero = new Warrior();
        Item item = new Weapon("Invalid_Axe", 1, ItemType.BOW, ItemSlot.WEAPON, 0, 0);
        Assertions.assertThrows(InvalidWeaponException.class, () -> hero.equipItem(item));
    }

    @Test
    public void HeroEquipItem_WrongTypeArmor_InvalidArmorException() {
        Hero hero = new Warrior();
        Item item = new Armor("Invalid_Axe", 1, ItemType.CLOTH_ARMOR, ItemSlot.BODY, new PrimaryAttributes());
        Assertions.assertThrows(InvalidArmorException.class, () -> hero.equipItem(item));
    }
    //endregion

    @Test
    public void HeroEquipItem_ValidWeapon_True() throws InvalidArmorException, InvalidWeaponException {
        Hero hero = new Warrior();
        Item item = new Weapon("Valid Axe", 1, ItemType.AXE, ItemSlot.WEAPON, 1, 1);
        Assertions.assertTrue(hero.equipItem(item));
    }

    @Test
    public void HeroEquipItem_ValidArmor_True() throws InvalidArmorException, InvalidWeaponException {
        Hero hero = new Warrior();
        Item item = new Armor("Valid Armor", 1, ItemType.PLATE_ARMOR, ItemSlot.BODY, new PrimaryAttributes());
        Assertions.assertTrue(hero.equipItem(item));
    }


    //region getterSettersTest
    @Test
    void HeroSetGetName_Name_NameChange(){
        Hero hero = new Warrior();
        String name = "Test name";
        hero.setName(name);
        Assertions.assertEquals(name,hero.getName());
    }
    @Test
    void HeroSetGetLevel_Level_LevelChange(){
        Hero hero = new Warrior();
        int level = 5;
        hero.setLevel(level);
        Assertions.assertEquals(level,hero.getLevel());
    }
    @Test
    void HeroSetGetPrimaryAttributes_PrimaryAttributes_LevelChange(){
        Hero hero = new Warrior();
        PrimaryAttributes primaryAttributes = new PrimaryAttributes(1,1,1);
        hero.setBasePrimaryAttributes(primaryAttributes);
        Assertions.assertEquals(primaryAttributes,hero.getBasePrimaryAttributes());
    }
    @Test
    void HeroSetGetPrimaryAttributes_PrimaryAttribute_LevelChange(){
        Hero hero = new Warrior();
        List<ItemType> itemTypeList = new ArrayList<>();
        itemTypeList.add(ItemType.AXE);
        itemTypeList.add(ItemType.MAIL_ARMOUR);
        hero.setHeroCanHaveItem(itemTypeList);
        Assertions.assertArrayEquals(itemTypeList.toArray(),hero.getHeroCanHaveItem().toArray());
    }
    @Test
    void HeroSetGet_PrimaryAttributes_LevelChange(){
        Hero hero = new Warrior();
        Armor armor = new Armor("Valid armor",1,ItemType.MAIL_ARMOUR, ItemSlot.BODY,new PrimaryAttributes(1,1,1));
        Weapon weapon = new Weapon("Valid Axe",1,ItemType.AXE,ItemSlot.WEAPON,1,1);
        Map<ItemSlot, Item> itemMap = new HashMap<>();
        itemMap.put(armor.getItemSlot(),armor);
        itemMap.put(weapon.getItemSlot(),weapon);
        hero.setEquiptItems(itemMap);
        Assertions.assertEquals(itemMap, hero.getEquiptItems());
    }
    //endregion

    
    @Test
    public void HeroLevelUp_Level_LevelTwo() {
        Hero hero = new Warrior();
        hero.levelUp();
        Assertions.assertEquals(2, hero.getLevel());
    }


    @Test
    public void HeroCalculateDPS_NoWeapon_OnePointOFive() {
        Hero hero = new Warrior();
        Assertions.assertEquals(1.05, hero.calculateDPS());
    }

    @Test
    public void HeroCalculateDPS_HasWeapon_EightPointOEEightyFive() throws InvalidArmorException, InvalidWeaponException {
        Hero hero = new Warrior();
        Item item = new Weapon("Valid Axe", 1, ItemType.AXE, ItemSlot.WEAPON, 7, 1.1);
        hero.equipItem(item);
        Assertions.assertEquals(8.085, hero.calculateDPS());
    }

    @Test
    public void HeroCalculateDPS_HasWEWeaponAndArmor_EightyPointFiveSevenOOne() throws InvalidArmorException, InvalidWeaponException {
        Hero hero = new Warrior();
        Item weapon = new Weapon("Valid Axe", 1, ItemType.AXE, ItemSlot.WEAPON, 7, 1.1);
        hero.equipItem(weapon);
        Armor armor = new Armor("Valid Armor", 1, ItemType.PLATE_ARMOR, ItemSlot.BODY, new PrimaryAttributes(1, 0, 0));
        hero.equipItem(armor);
        Assertions.assertEquals(8.162, hero.calculateDPS());
    }

    @Test
    void HeroDisplayStats_ReturnString_ContainsImportantStats(){
        Hero hero = new Warrior();
        hero.setName("JUnit");
        List<String> checkStrings = new ArrayList<>(Arrays.asList(
                "Character name: JUnit", "Character level: 1","Strength: 5",
                "Dexterity: 2","Intelligence: 1","Hero dps: 1.05"));
        String returnString = hero.displayStats();
        for (String check: checkStrings) {
            Assertions.assertTrue(returnString.contains(check));
        }
    }

}
