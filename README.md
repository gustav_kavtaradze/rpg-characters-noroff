# RPG Characters

Assignment
Build a console application in Java.

A simple RPG character creation maker. Part of Noroff Java fundamentals

## Description

Using this application a user can create a Hero with different classes (Mage, Ranger, Rogue 
and Warrior). 
That depending on class will decide how hero levels up and what attribute increases hero damage.

Heroes can also equip different types of items (weapons and armour) that are class specific and 
can also increase heroes attribute. 

NOTE:

The master branch of this project is made to meet the requirements from Noroff assignment 1 in Java Winter 22.
Using JUnit to make unit testing.
To get a functioning character create command application use the "working-character-maker"
branch.

## Getting Started

### Dependencies

* JDK (to project in IDE)
* JUnit 5.8.1 (added to class path when cloning project)
* Java IDE (IntelliJ or any other modern Java IDE)

### Installing

* Clone project
* (Make sure JUnit is added)
* Open project in IDE and build

### Executing program

* Run tests by right-clicking test folder and runt all tests.

* To run "working-character-maker" branch open project and runt Main.java file.

## Authors

Gustav Eklund Kavtaradze [@gustav_kavtaradze]


## Acknowledgments

Inspiration for unit testing and some implementations (ItemSlot, ItemType) from Noroff assignment pdf.
